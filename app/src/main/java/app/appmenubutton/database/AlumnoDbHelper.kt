package app.appmenubutton.database

import android.content.ContentValues
import android.content.Context

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class AlumnoDbHelper(private val context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        private const val DATABASE_NAME = "sistema.db"
        private const val DATABASE_VERSION = 1
        private const val TEXT_TYPE = " TEXT"
        private const val INTEGER_TYPE = " INTEGER"
        private const val COMA = ","
        private const val SQL_CREATE_ALUMNO =
            "CREATE TABLE " +
                    DefinirDB.Alumnos.TABLA +
                    "(${DefinirDB.Alumnos.ID}$INTEGER_TYPE PRIMARY KEY $COMA" +
                    "${DefinirDB.Alumnos.MATRICULA}$TEXT_TYPE$COMA" +
                    "${DefinirDB.Alumnos.NOMBRE}$TEXT_TYPE$COMA" +
                    "${DefinirDB.Alumnos.DOMICILIO}$TEXT_TYPE$COMA" +
                    "${DefinirDB.Alumnos.ESPECIALIDAD}$TEXT_TYPE$COMA" +
                    "${DefinirDB.Alumnos.FOTO}$TEXT_TYPE)"
        private const val SQL_DELETE_ALUMNO = "DROP TABLE IF EXISTS ${DefinirDB.Alumnos.TABLA}"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SQL_CREATE_ALUMNO)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(SQL_DELETE_ALUMNO)
        onCreate(db)
    }

    fun insertarAlumno(alumno: Alumno): Long {
        val db = this.writableDatabase
        val contentValues = ContentValues().apply {
            put(DefinirDB.Alumnos.MATRICULA, alumno.matricula)
            put(DefinirDB.Alumnos.NOMBRE, alumno.nombre)
            put(DefinirDB.Alumnos.DOMICILIO, alumno.domicilio)
            put(DefinirDB.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(DefinirDB.Alumnos.FOTO, alumno.foto) // Guardar la ruta de la imagen
        }
        val id = db.insert(DefinirDB.Alumnos.TABLA, null, contentValues)
        db.close()
        return id
    }

    fun actualizarAlumno(alumno: Alumno, id: Int): Int {
        val db = this.writableDatabase
        val contentValues = ContentValues().apply {
            put(DefinirDB.Alumnos.MATRICULA, alumno.matricula)
            put(DefinirDB.Alumnos.NOMBRE, alumno.nombre)
            put(DefinirDB.Alumnos.DOMICILIO, alumno.domicilio)
            put(DefinirDB.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(DefinirDB.Alumnos.FOTO, alumno.foto) // Guardar la ruta de la imagen
        }
        val rowsAffected = db.update(DefinirDB.Alumnos.TABLA, contentValues, "${DefinirDB.Alumnos.ID} = ?", arrayOf(id.toString()))
        db.close()
        return rowsAffected
    }


    fun getAlumnoByMatricula(matricula: String): Alumno {
        val db = this.readableDatabase
        val cursor: Cursor = db.query(DefinirDB.Alumnos.TABLA, null, "${DefinirDB.Alumnos.MATRICULA} = ?", arrayOf(matricula), null, null, null)
        val alumno = if (cursor.moveToFirst()) {
            Alumno(
                id = cursor.getInt(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.ID)),
                matricula = cursor.getString(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.MATRICULA)),
                nombre = cursor.getString(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.NOMBRE)),
                domicilio = cursor.getString(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.DOMICILIO)),
                especialidad = cursor.getString(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.ESPECIALIDAD)),
                foto = cursor.getString(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.FOTO))
            )
        } else {
            Alumno()
        }
        cursor.close()
        db.close()
        return alumno
    }

    fun obtenerTodosLosAlumnos(): List<Alumno> {
        val alumnosList = ArrayList<Alumno>()
        val db = this.readableDatabase
        val cursor: Cursor = db.query(DefinirDB.Alumnos.TABLA, null, null, null, null, null, null)

        while (cursor.moveToNext()) {
            val alumno = Alumno(
                id = cursor.getInt(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.ID)),
                matricula = cursor.getString(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.MATRICULA)),
                nombre = cursor.getString(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.NOMBRE)),
                domicilio = cursor.getString(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.DOMICILIO)),
                especialidad = cursor.getString(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.ESPECIALIDAD)),
                foto = cursor.getString(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.FOTO))
            )
            alumnosList.add(alumno)
        }
        cursor.close()
        db.close()

        return alumnosList.map { alumno ->
            // Mapear la URL de la foto correctamente
            if (alumno.foto.isNotEmpty() && alumno.foto != "0") {
                // Si la URL no está vacía y no es "0", usar la URL proporcionada
                alumno.copy(foto = alumno.foto)
            } else {
                // Si no hay URL válida, asignar la imagen predeterminada
                alumno.copy(foto = "0")
            }
        }
    }



    fun borrarAlumno(id: Int): Int {
        val db = this.writableDatabase
        val rowsAffected = db.delete(DefinirDB.Alumnos.TABLA, "${DefinirDB.Alumnos.ID} = ?", arrayOf(id.toString()))
        db.close()
        return rowsAffected
    }
}