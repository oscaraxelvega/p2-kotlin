package app.appmenubutton.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase

class dbAlumnos(private val context: Context) {
    private val dbHelper: AlumnoDbHelper = AlumnoDbHelper(context)

    private lateinit var db: SQLiteDatabase

    private val leerRegistro = arrayOf(
        DefinirDB.Alumnos.ID,
        DefinirDB.Alumnos.MATRICULA,
        DefinirDB.Alumnos.NOMBRE,
        DefinirDB.Alumnos.DOMICILIO,
        DefinirDB.Alumnos.ESPECIALIDAD
    )

    fun openDataBase() {
        db = dbHelper.writableDatabase
    }

    fun insertarAlumno(alumno: Alumno): Long {
        val value = ContentValues().apply {
            put(DefinirDB.Alumnos.MATRICULA, alumno.matricula)
            put(DefinirDB.Alumnos.NOMBRE, alumno.nombre)
            put(DefinirDB.Alumnos.DOMICILIO, alumno.domicilio)
            put(DefinirDB.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(DefinirDB.Alumnos.FOTO, alumno.foto)
        }
        return db.insert(
            DefinirDB.Alumnos.TABLA, null, value
        )
    }

    fun actualizarAlumno(alumno: Alumno, id: Int): Int {
        val values = ContentValues().apply {
            put(DefinirDB.Alumnos.MATRICULA, alumno.matricula)
            put(DefinirDB.Alumnos.NOMBRE, alumno.nombre)
            put(DefinirDB.Alumnos.DOMICILIO, alumno.domicilio)
            put(DefinirDB.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(DefinirDB.Alumnos.FOTO, alumno.foto)
        }
        return db.update(
            DefinirDB.Alumnos.TABLA,
            values,
            "${DefinirDB.Alumnos.ID} = ?",
            arrayOf(id.toString())
        )
    }

    fun borrarAlumno(id: Int): Int{
        return db.delete(DefinirDB.Alumnos.TABLA,"${DefinirDB.Alumnos.ID} = ?", arrayOf(id.toString()))
    }

    fun mostrarAlumnos(cursor: Cursor): Alumno {
        return try {
            Alumno().apply {
                id = cursor.getInt(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.ID))
                matricula = cursor.getString(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.MATRICULA))
                nombre = cursor.getString(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.NOMBRE))
                domicilio = cursor.getString(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.DOMICILIO))
                especialidad = cursor.getString(cursor.getColumnIndexOrThrow(DefinirDB.Alumnos.ESPECIALIDAD))

                // Verifica si la columna de foto existe y maneja posibles valores nulos
                val fotoIndex = cursor.getColumnIndex(DefinirDB.Alumnos.FOTO)
                if (fotoIndex != -1 && !cursor.isNull(fotoIndex)) {
                    foto = cursor.getString(fotoIndex)
                } else {
                    foto = ""
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Alumno() // Retorna un objeto Alumno vacío en caso de error
        }
    }

    fun getAlumno(id: Long): Alumno {
        val db = dbHelper.readableDatabase
        val cursor = db.query(
            DefinirDB.Alumnos.TABLA,
            leerRegistro,
            "${DefinirDB.Alumnos.ID} = ?",
            arrayOf(id.toString()), null, null, null
        )
        cursor.moveToFirst()
        val alumno = mostrarAlumnos(cursor)
        cursor.close()
        return alumno
    }
    fun getAlumnoByMatricula(matricula: String): Alumno {
        val db = dbHelper.readableDatabase
        val cursor = db.query(
            DefinirDB.Alumnos.TABLA,
            leerRegistro,
            "${DefinirDB.Alumnos.MATRICULA} = ?",
            arrayOf(matricula), null, null, null
        )
        if (cursor.moveToFirst()){
            val alumno = mostrarAlumnos(cursor)
            cursor.close()
            return alumno
        }else return Alumno()
    }

    fun leerTodos(): ArrayList<Alumno> {
        val cursor = db.query(DefinirDB.Alumnos.TABLA, leerRegistro, null, null, null, null, null)
        val listaAlumnos = ArrayList<Alumno>()
        cursor.moveToFirst()
        while (!cursor.isAfterLast){
            val alumno = mostrarAlumnos(cursor)
            listaAlumnos.add(alumno)
            cursor.moveToNext()
        }
        cursor.close()
        return listaAlumnos
    }

    fun close (){
        dbHelper.close()
    }
}